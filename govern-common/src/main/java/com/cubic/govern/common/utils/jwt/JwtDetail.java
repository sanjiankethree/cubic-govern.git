package com.cubic.govern.common.utils.jwt;

import io.jsonwebtoken.Claims;
import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.util.Date;

/**
 * @ClassName JwtDetail
 * @Author QIANGLU
 * @Date 2020/11/18 5:03 下午
 * @Version 1.0
 */
@Data
@Builder
public class JwtDetail {

    /**
     * client唯一标识,或username
     */
    private String id;


    private Claims claims;

    /**
     * 过期时间
     */
    private Date exDate;


    /**
     * token是否过期
     */
    private boolean isExpired;

    /**
     * 签名
     */
    private String sign;

    @Tolerate
    JwtDetail() {
    }
}
