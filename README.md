# cubic-govern

## 简介
基于Spring Cloud 、Security、JWT技术体系的微服务接口认证授权平台，
目前公司内部服务授权都是基于依赖包进行服务的授权，
为了减少真实场景下的依赖支持基于agent的认证授权机制，
使项目无缝接入，迭代开发快到飞起来。

目前正在封闭快速迭代中。。。

## 软件架构
先放一个基础架构
![输入图片说明](https://images.gitee.com/uploads/images/2020/1123/183121_b11e6137_1168339.jpeg "服务认证.jpg")

 
## 技术体系
>因为我比较喜欢追新，所以未来都会使用最新版

| 名称               | 版本                                            |
|------------------|------------------------------------------------|
| Spring Cloud       | Hoxton.SR8                         |
| spring-boot | 2.3.5.RELEASE                         |
| spring-cloud-alibaba-dependencies     | 2.2.1.RELEASE                                  |
| jjwt          | 0.9.1                                    |
| swagger-spring-boot-starter     | 1.9.1.RELEASE                                       |
| mybatis-plus-boot-starter     | 3.4.1                                       |
| druid-spring-boot2-starter      | 1.1.10.2                               |
 

 ## 环境需求
 - JDK1.8
 - Maven 3.0
 - MySQL 5.5

## 项目结构
- govern-auth   认证授权平台
- govern-auth-agent 可进行本地认证的agent
- govern-common  基础组件
- govern-ops 运维管理
- govern-service-api  公共接口

## 本地部署

1、git clone https://gitee.com/sanjiankethree/cubic-govern.git
2、运行 govern-auth 下的 CubicAuthApplication 启动应用


## 当前状态
 - 12.02 增加前端页面结构
 
## 接口文档（补充中）
https://docs.apipost.cn/view/8cf69afcd660b731
