package com.cubic.auth.runner;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.cubic.auth.entity.Client;
import com.cubic.auth.entity.User;
import com.cubic.auth.mapper.ClientMapper;
import com.cubic.auth.mapper.UserMapper;
import com.cubic.auth.store.DataStore;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * 用于服务启动时将服务认证信息加载到内存
 * @ClassName DataStoreRunner
 * @Author QIANGLU
 * @Date 2020/11/19 15:00
 * @Version 1.0
 */
@Component
public class DataStoreRunner implements CommandLineRunner {

    @Resource
    private ClientMapper clientMapper;

    @Resource
    private DataStore dataStore;

    @Override
    public void run(String... args) throws Exception {

        QueryWrapper wrapper = new QueryWrapper();
        List<Client> clients = clientMapper.selectList(wrapper);
        if (clients != null) {
            clients.forEach(client -> {
                dataStore.register(client.getClientId(), client.getToken());
            });
        }

    }
}
