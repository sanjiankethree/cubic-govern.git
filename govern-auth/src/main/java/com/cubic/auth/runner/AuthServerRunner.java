package com.cubic.auth.runner;

import com.cubic.auth.config.JwtProperties;
import com.cubic.govern.common.utils.jwt.RsaKeyHelper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Configuration;

import javax.annotation.Resource;
import java.util.Map;

/**
 * 用于启动前加载jwt相关参数
 * @author luqiang
 */
@Configuration
public class AuthServerRunner implements CommandLineRunner {


    @Resource
    private JwtProperties jwtProperties;

    @Override
    public void run(String... args) throws Exception {

        Map<String, byte[]> keyMap = RsaKeyHelper.generateKey(jwtProperties.getRsaSecret());
        jwtProperties.setPrivateKey(keyMap.get("pri"));
        jwtProperties.setPublicKey(keyMap.get("pub"));

    }
}
