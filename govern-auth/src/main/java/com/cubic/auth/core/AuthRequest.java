package com.cubic.auth.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @author luqiang
 */
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthRequest implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * client唯一标识
     */
    private String clientId;

    /**
     * 授权码
     */
    private String verCode;


}
