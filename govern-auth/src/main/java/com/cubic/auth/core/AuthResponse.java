package com.cubic.auth.core;

import lombok.Builder;
import lombok.Data;
import lombok.experimental.Tolerate;

import java.io.Serializable;

/**
 * @author luqiang
 */
@Data
@Builder
public class AuthResponse implements Serializable {
    private static final long serialVersionUID = 1250166508152483573L;

    /**
     * 状态码
     */
    private String code ;

    /**
     * 信息
     */
    private String msg;
    /**
     * token
     */
    private String token;

    @Tolerate
    AuthResponse() {
    }

}
