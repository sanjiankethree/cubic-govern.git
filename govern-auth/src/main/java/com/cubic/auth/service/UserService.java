package com.cubic.auth.service;


import com.cubic.auth.core.AuthRequest;
import com.cubic.auth.entity.User;
import com.cubic.auth.vo.LoginDetail;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author luqiang
 */
public interface UserService extends UserDetailsService {


}
