package com.cubic.auth.service;

import com.cubic.auth.core.AuthRequest;
import com.cubic.auth.entity.User;
import com.cubic.auth.mapper.UserMapper;
import com.cubic.auth.utils.JwtTokenUtil;
import com.cubic.auth.vo.LoginDetail;
import com.cubic.govern.common.utils.jwt.JwtDetail;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.impl.DefaultClaims;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;
import java.util.Date;

/**
 * @ClassName UserServiceImpl
 * @Author QIANGLU
 * @Date 2020/11/18 2:25 下午
 * @Version 1.0
 */
@Slf4j
@Service
public class SystemServiceImpl implements SystemService {

    @Resource
    private UserMapper userMapper;

    @Resource
    private ClientService clientService;

    @Override
    public String login(LoginDetail loginDetail)  {

        String username = loginDetail.getUsername();

        AuthRequest request = AuthRequest.builder().clientId(username).verCode(loginDetail.getCaptcha()).build();

        return clientService.getToken(request);
    }
}
