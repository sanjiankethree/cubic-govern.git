package com.cubic.auth.service;


import com.cubic.auth.vo.LoginDetail;
import org.springframework.security.core.userdetails.UserDetailsService;

/**
 * @author luqiang
 */
public interface SystemService {

    /**
     * 验证登录
     *
     * @param loginDetail
     * @return
     */
    String login(LoginDetail loginDetail);


}
