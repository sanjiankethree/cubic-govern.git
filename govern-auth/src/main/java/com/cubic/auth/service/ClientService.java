package com.cubic.auth.service;

import com.cubic.auth.core.AuthRequest;

/**
 * @ClassName ClientService
 * @Author QIANGLU
 * @Date 2020/11/25 10:39
 * @Version 1.0
 */
public interface ClientService {

    /**
     * 根据请求获取token
     * @param request
     * @return
     */
    String getToken(AuthRequest request) ;

    /**
     * 注册client
     * @param request
     * @return
     */
    String register(AuthRequest request);

}
