package com.cubic.auth.controller;

import com.alibaba.fastjson.JSON;
import com.cubic.auth.core.AuthRequest;
import com.cubic.auth.core.AuthResponse;
import com.cubic.auth.service.ClientService;
import com.cubic.auth.service.UserService;
import com.cubic.auth.utils.ResponseCode;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author luqiang
 */
@RestController
@RequestMapping("/auth")
@Slf4j
public class AuthController {


    @Resource
    private ClientService clientService;


    /**
     * 获取token
     * @param authRequest
     * @return
     */
    @RequestMapping(value = "/getToken", method = RequestMethod.POST)
    public String getToken(@RequestBody AuthRequest authRequest) {

        String token = clientService.getToken(authRequest);

        AuthResponse response = AuthResponse.builder().code(ResponseCode.SUCC.getCode()).token(token).build();
        return JSON.toJSONString(response);
    }

    /**
     * 启动注册
     * @param authRequest
     * @return
     */
    @RequestMapping(value = "/register", method = RequestMethod.POST)
    public String register(@RequestBody AuthRequest authRequest) {

        String token = clientService.getToken(authRequest);

        AuthResponse response = AuthResponse.builder().code(ResponseCode.SUCC.getCode()).token(token).build();
        return JSON.toJSONString(response);
    }

    @RequestMapping(value = "/refreshToken", method = RequestMethod.GET)
    public String refreshToken(
            @RequestBody AuthRequest authRequest) throws Exception {

        return null;
    }

    @RequestMapping(value = "/verifyToken", method = RequestMethod.GET)
    public String verifyToken(@RequestBody AuthRequest authRequest) throws Exception {

        return null;
    }


}
