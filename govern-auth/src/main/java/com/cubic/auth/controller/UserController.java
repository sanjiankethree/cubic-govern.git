package com.cubic.auth.controller;

import com.cubic.auth.core.AuthRequest;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * @ClassName SystemController
 * @Author QIANGLU
 * @Date 2020/11/18 2:49 下午
 * @Version 1.0
 */
@RestController
@RequestMapping("/user")
public class UserController {


    @RequestMapping("/list")
    public String list() {
        return " SUCC";
    }


    @RequestMapping(value = "/logout", method = RequestMethod.DELETE)
    public String logout(@RequestBody AuthRequest authRequest) throws Exception {
        return null;
    }

}
