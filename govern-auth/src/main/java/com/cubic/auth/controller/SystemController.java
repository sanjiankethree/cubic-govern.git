package com.cubic.auth.controller;

import com.cubic.auth.jwt.GovernJwtAuthenticationToken;
import com.cubic.auth.service.SystemService;
import com.cubic.auth.service.UserService;
import com.cubic.auth.vo.LoginDetail;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @ClassName LoginController
 * @Author QIANGLU
 * @Date 2020/11/18 2:49 下午
 * @Version 1.0
 */
@RestController
@RequestMapping("/sys")
public class SystemController {


    @Resource
    private SystemService systemService;

    @Resource
    private AuthenticationManager authenticationManager;


    @RequestMapping("/login")
    public String login(@RequestBody LoginDetail loginDetail) {

        GovernJwtAuthenticationToken token = new GovernJwtAuthenticationToken(loginDetail.getUsername(), loginDetail.getPassword());
        Authentication authentication = authenticationManager.authenticate(token);
        SecurityContextHolder.getContext().setAuthentication(authentication);
        return systemService.login(loginDetail);
    }


    @RequestMapping("/loginFail")
    public String loginFail() {
        return "login fail";
    }
}
