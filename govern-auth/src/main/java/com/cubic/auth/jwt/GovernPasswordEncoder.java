package com.cubic.auth.jwt;

import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * @ClassName GovernPasswordEncoder
 * @Author QIANGLU
 * @Date 2020/11/25 12:20
 * @Version 1.0
 */
public class GovernPasswordEncoder implements PasswordEncoder {
    @Override
    public String encode(CharSequence rawPassword) {
        return rawPassword.toString();
    }

    @Override
    public boolean matches(CharSequence rawPassword, String encodedPassword) {
        return rawPassword.toString().equals(encodedPassword);
    }
}
