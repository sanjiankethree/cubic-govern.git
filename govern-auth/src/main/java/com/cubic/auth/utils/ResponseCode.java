package com.cubic.auth.utils;

/**
 * @ClassName ResponseCode
 * @Author QIANGLU
 * @Date 2020/11/18 4:31 下午
 * @Version 1.0
 */
public enum ResponseCode {


    /**
     * 成功
     */
    SUCC("200");

    private String code;

    private ResponseCode(String code) {
        this.code = code;
    }

    public String getCode() {
        return this.code;
    }
}
