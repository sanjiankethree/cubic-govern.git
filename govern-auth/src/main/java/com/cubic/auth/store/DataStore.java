package com.cubic.auth.store;

/**
 * @ClassName DataStore
 * @Author QIANGLU
 * @Date 2020/11/19 14:03
 * @Version 1.0
 */
public interface DataStore {

    /**
     * 注册缓存数据
     * @param id 唯一标识
     * @param token token
     */
    void register(String id,String token);

    /**
     * 移除token
     * @param id
     */
    void remove(String id);

    /**
     * 获取token
     * @param id
     */
    String get(String id);
}
