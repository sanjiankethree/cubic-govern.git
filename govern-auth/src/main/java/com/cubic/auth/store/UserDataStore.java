package com.cubic.auth.store;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import java.util.concurrent.ConcurrentHashMap;

/**
 * @ClassName DataStore
 * @Author QIANGLU
 * @Date 2020/11/19 14:03
 * @Version 1.0
 */
@Slf4j
@Component
public class UserDataStore implements DataStore{

    private static final ConcurrentHashMap<String,String> USER_TOKEN = new ConcurrentHashMap<>();

    public UserDataStore(){
    }

    /**
     * 注册缓存数据
     * @param id 唯一标识
     * @param token token
     */
    @Override
    public void register(String id, String token) {
        USER_TOKEN.put(id,token);
    }

    /**
     * 移除token
     * @param id
     */
    @Override
    public void remove(String id){
        USER_TOKEN.remove(id);
    }

    @Override
    public String get(String id) {
       return USER_TOKEN.get(id);
    }
}
