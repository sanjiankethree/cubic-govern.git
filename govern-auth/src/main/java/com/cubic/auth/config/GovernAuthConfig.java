package com.cubic.auth.config;


import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

/**
 * @ClassName MatrixSecurityConfig
 * @Author QIANGLU
 * @Date 2020/11/17 4:09 下午
 * @Version 1.0
 */
@Configuration
public class GovernAuthConfig extends WebMvcConfigurationSupport {

    @Override
    protected void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
                .allowedOrigins("*")
                .allowedMethods("*")
                .maxAge(16800)
                .allowedHeaders("*")
                .allowCredentials(true);
    }
}
